﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;

namespace nhibernateTest
{
    public class SessionFactoryBuilder
    {


        public static ISessionFactory BuildSessionFactory(string connectionStringName)
        {
            var connectionString = System.Configuration.ConfigurationManager
                .ConnectionStrings[connectionStringName].ConnectionString;

            return Fluently.Configure()
//                .Diagnostics(c => c.Enable(true).OutputToConsole()) // If you want to see logs on console - for debug
                .Database(PostgreSQLConfiguration.Standard.ConnectionString(connectionString))
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<UserMap>())
                .BuildSessionFactory();
        }
    }
}