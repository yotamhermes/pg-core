﻿using System;

namespace nhibernateTest
{
    public class User
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Mail { get; set; }
        public virtual DateTime Birthday { get; set; }
    }
}