﻿using System;

namespace nhibernateTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionStringName = "postgresql";
            var sessionFactory = SessionFactoryBuilder.BuildSessionFactory(connectionStringName);

            using (var session = sessionFactory.OpenSession())
            {
                var x = session.QueryOver<User>().List();
            }

            Console.ReadLine();
        }
    }
}
