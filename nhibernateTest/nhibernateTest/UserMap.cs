﻿using FluentNHibernate.Mapping;

namespace nhibernateTest
{
    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {            
            Table("users");
            Id(x => x.Id, "user_id");
            Map(x => x.Name,"username");
            Map(x => x.Mail, "email");
            Map(x => x.Birthday, "birthday");
        }
    }
}