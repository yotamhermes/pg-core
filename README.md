# pg-core

This is a simple example for .net core use with fluent nhibernate and postgresql configured.

Make sure to replace the **connectionString** in the right format in App.config

`connectionString = "Host=YOUR_HOST;Database=YOUR_DATABASE;Password=YOUR_PASSWORD;Username=YOUR_USERNAME;Persist Security Info=True"`

